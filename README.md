# unix machine setup

```bash
sudo apt-get install -y git vim python3-dev python-pip tmux zsh virtualenv
pip install --upgrade pip
pip install wheel setuptools virtualenvwrapper
```

## Vim setup
```bash
cp .vimrc ~/
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

Then in vim
```Vim
:PluginInstall
```

## Zshrc setup
```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
cp .zshrc ~/
source ~/.zshrc
```

## tmux setup
```bash
cp .tmux.conf ~/
```
