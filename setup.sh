#!/bin/sh

if [ $(id -u) = 0 ]
then
    echo "\033[0;33m You must not run this as root\033[0;37m"
    exit
fi

ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa -q -N ""
xclip -sel clip < ~/.ssh/id_rsa.pub || {
    echo "\033[0;33m Failed to copy public key to clipboard, copy it manually, then continue \033[0;37m"
    echo ""
    cat ~/.ssh/id_rsa.pub
    echo ""
    }

echo ""
echo "Hit return to continue"
read dummy


git clone git@gitlab.com:it25/unix_setup.git ~/dotfiles
cd ~/dotfiles
cp -t ~ .vimrc .tmux.conf .zshrc
cd ..
rm -rf dotfiles

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall

# Note: Using a forked version of the original script.
# Check back regularly for the Pull request's merging
sh -c "$(curl -fsSL https://raw.githubusercontent.com/Liquidsoul/oh-my-zsh/f4331ee5a57215e3c485dc10ff3572c0145300e9/tools/install.sh) --silent"


echo "All done! Don't forget to 'source ~/.zshrc' if you don't feel like restarting your user session"