
# ::: OhmyZSH :::

# Path to your oh-my-zsh installation.
  export ZSH=$HOME/.oh-my-zsh
# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="crunch"
# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(docker git virtualenv virtualenvwrapper)

source $ZSH/oh-my-zsh.sh
CRUNCH_PROMPT='%{$reset_color%}➤ '
HOST_PROMPT_='%n@%m:%{$fg[$NCOLOR]%}'
PROMPT="$CRUNCH_TIME_$CRUNCH_RVM_$HOST_PROMPT_$CRUNCH_DIR_$CRUNCH_PROMPT%{$reset_color%}"

# ::: User configuration :::

export EDITOR=vim

bindkey "\e$terminfo[kcub1]" backward-word
bindkey "\e$terminfo[kcuf1]" forward-word
