#!/bin/sh

if [ $(id -u) != 0 ]
then
    echo "\033[0;31m You must run this with sudo"
    exit
fi

apt install -y curl ssh git xclip vim python3-dev python-pip tmux zsh virtualenv 
pip install pipenv